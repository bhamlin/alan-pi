# Alan (Turing) Pi

Rust based interface for Turing Pi 2 BMC.

Intended to build for the BMC itself with target `armv7-unknown-linux-musleabi`.
